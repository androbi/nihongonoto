# Vocabulary

* [Adjectives](Adjectives.md)
* [Verbs](Verbs.md)
* [Days of the month](Days.md)
* [Counters](Counters.md)
* [Family](Family.md)
* [Hiragana and Katakana](50sounds.md)
* [New words](Newwords.md)