### New Words

 英語|日本語 |ひらがな
---|---|---
(daily) live, livelihood| 生活 | せいかつ
serious, honest | 真面目 | まじめ(な)
for example | 例えば | たとえば
swimming | 水泳  | すいえい  
twice a week | 一週間に２回  |  いっしゅうかん　に　にかい
work by using computer | パソコンを使って仕事をします | 
by myself | 自分で | じぶんで
husband (informal) | 旦那 | だんな
to go out | 出かける | 
trad. jap. food | 懐石料理 | かいせきりょうり
full with cars | 車がいっぱい |
rest, break | 休憩 | きゅうけい
lunch | お昼ご飯 | おひるごはん
to become hungry | お腹が減る | おなかがへる
after work | 仕事のあと | しごとのあと
work too much | 働きすぎです | はたらきすぎです
everything | 全て | すべて
same | 同じ | おなじ
not yet | まだ |
potatoes | じゃがいも |
dialect | 方言 | ほうげん
to smoke | タバコをすう |
Smoking Prohibited! | きんえん | 
important | 大切 | たいせつ
wind, flue | 風| かぜ
vegetables | 野菜 | やさい 
fruits | 果物　| くだもの
to return something | 返す | かえす
to bring along, to go with | 連れる | つれる
to bring someone | 連れてくる | つれてくる
to hold | 持つ | もつ
to bring (things) | 持ってくる | もってくる
to sit | 座る | すわる
to hurry | 急ぐ | いそぐ
to close | 閉める | しめる
past | 過去 | かこ
throw away, give up | 捨てる | すてる
to know | 知る | しる (u-verb)
to call | かける
to make, to produce | 作る | つくる
to wash | 洗う | あらう 
to memorize | 覚える | おぼえる
to launder | 洗濯する | せんたくする
to exercise | 運動する | うんどうする
attractive, cool | 格好いい | かっこいい
special | とくべつな
Reiwa | 令和 | れいわ
scary | こわい
till next time | また今度 | またこんど
slowly (speak) | ゆっくり
to borrow, to loan | 借りる | かりる
to bath | お風呂に入る | おふろに入る
newspaper​ | 新聞 | しんぶん
to turn on (light) | つける