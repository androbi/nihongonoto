### Common verbs

##### 五段 (u - verbs)

英語|日本語|ローマ字
--- | --- |---
to meet | 会う | au
to drink | 飲む | nomu
to buy | 買う | kau
to read | 読む | yomu
to pay | 払う | harau
to request | 頼む | tanomu
to sing | 歌う | utau
to sit |座る | suwaru
to wash | 洗う | arau
to take | 取る | toru
to say | 言う | iu
to go up | 上る | noboru
to laugh | 笑う | warau
to get on train | 乗る | noru
to help | 手伝う | tetsudau
to dance | 踊る | odoru
to hear | 聞く | kiku
to make | 作る |tsukuru
to write | 書く | kaku
to send | 送る | okuru
to go | 行く | iku
to enter | 入**る** | hairu
to work | 働く | hataraku
to run | 走**る** | hashiru
to walk | 歩く | aruku
to need | い**る** | iru
to talk |　話す | hanasu
to chatter | しゃべ**る** | shaberu
to cut | 切**る** | kiru
to erase | 消す | kesu
to return, come back | 帰**る** | kaeru
to return (something)| 返す | kaesu
to swim | 泳ぐ | oyogu
to wait | 待つ | matsu
to choose | 選ぶ | erabu
to stand up | 立つ | tatsu
to carry | 運ぶ | hacobu
to die | 死ぬ | shinu
to know | 知**る** | shiru

u-verbs that end in いる or える are marked with **る**. See http://www.guidetojapanese.org/verbs.html

##### 一段 (iru/eru - verbs)

英語|日本語|ローマ字
----|----|----
to see | 見る | miru 
to start | 始める | hajimeru
to get up/awake | 起きる | okiru
to teach | 教える | oshieru
to descend | 降りる | oriru
to lie down | 寝る | neru
to believe| 信じる | shinjiru
to forget| 忘れる | wasureru
to be (animate)| 居る | iru
to answer| 答える | kotaeru
to wear| 着る | kiru
to go out|出かける |dekakeru
to eat|食べる|taberu
to open|開ける|akeru

##### irregular

英語|日本語|ローマ字
----| ----|----
to come | 来る | kuru
to do | する | suru


##### Transitive and intransitive

英語|(t)|(i) 
----|---|---
drop|落(お)とす|落(お)ちる
exit|出(だ)す|出(で)る
enter|入(い)れる|入(はい)る
open|開(あ)ける|開(あ)く
close|閉(し)める|閉(し)まる
turn on|つける|つく
turn off|消(け)す|消(き)える
pull,skip|抜(ぬ)く|抜(ぬ)ける
break|壊(こわ)す|壊れる
