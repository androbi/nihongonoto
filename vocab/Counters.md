### Counters

じ|usage
---|---
部|Copies of a magazine/newspaper
{台;だい}|Cars, bicycles, machines
杯|Cups and glasses of drink, spoonsful
匹|Small animals
本|Long, thin objects
階|floors, stories
個|small and/or round items
枚|thin, flat objects
名|People (polite)
{人;にん}|People exceptions 一人 and 二人
冊|Books
つ|General-purpose counter
{話;わ}|stories, episodes of TV series, etc.
{歳;さい}(才)|age of living creatures
回|number of times
ヶ所|number of locations


[wiki](https://de.m.wikipedia.org/wiki/Liste_japanischer_Z%C3%A4hlw%C3%B6rter?wprov=sfla1)

