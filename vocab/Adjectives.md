### Common adjectives

##### い adjectives

英語|日本語|ローマ字
---|---|---
blue|青い|aoi
red|赤い|akai
light, bright|明るい|akarui
warm|温かい|atatakai
new|新しい|atarashii
hot (air)|暑い|atsui
thick|厚い|atsui
dangerous|危ない|abunai
sweet|甘い|amai
good|良い|yoi
to be busy|忙し|isogashi
to be painful|痛い|itai
thin|薄い|usui
tasty, delicious|美味しい|oishi
big|大きい|ooki
late, slow|遅い|osoi
heavy|重い|omoi
interesting, funny|面白い|omoshiroi
hot, spicy|辛い{からい}|karai
light (not heavy)|軽い|karui
cute, pretty|可愛い|kawaii
yellow|黄色い|kiiroi
dirty|汚い|kitanai
dark|暗い|kurai
black|黒い|kuroi
cold|寒い|samui
white|白い|shiroi
cool|涼しい|suzushii
narrow|狭い|semai
high, expensive|高い|takai
pleasant, enjoyable|楽しい|tanoshii
small|小さい|chiisai
near,close|近い|chikai
uninteresting|詰らない{つまらない}|tsumaranai
cold|冷たい|tsumetai
strong|強い|tsuyoi
far|遠い|tooi
long|長い|nagai
early|早い|hayai
fast, quick|速い|hayai
low|低い|hikui
wide, spacious|広い|hiroi
thick, fat|太い|futoi
old|古い|furui
to want something|欲しい|hoshii
thin, fine|細い|hosoi
bad tasting|不味い|mazui
round|丸い|marui
short|短い|mijikai
difficult|難しい|muzukashii
gentle|優しい|yasashii
cheap|安い|yasui
young|若い|wakai

##### な adjectives

英語|日本語|ローマ字
---|---|---
To be quiet | 静かだ | Shizuka da 
To be lively | 元気だ | Genki da  
To be easy | 簡単だ |  Kantan da  
To be pretty | 綺麗だ | Kirei da  
To be indispensable | 大切だ | Taisetsu da  
To be serious | 真面目だ | Majime da  
To be strange | 変だ | Hen da  
To be cool/calm | 平気だ | Heiki da  
To be famous | 有名だ | Yūmei da  
To be kind | 親切だ | Shinsetsu da  
To be good at | 得意だ | Tokui da  
To be convenient | 便利だ | Benri da  
To be inconvenient | 不便だ | Fuben da  
To be handsome | ハンサムだ | Hansamu da  
To be chic | シックだ | Shikku da 