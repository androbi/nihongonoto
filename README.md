# 日本語のノート

* [Conjugation](conjugation/README.md)
    * [Forms](conjugation/Forms.md)
    * [Adjectives](conjugation/Adjectives.md)
    * [Verbs](conjugation/Verbs.md)
* [Constructs](constructs/README.md)
    * [に 行く/来る](constructs/niiku.md)
    * [Adj+する/なる](constructs/Surunaru.md)
    * [Compound sentences](constructs/Compound.md)
    * [Experience and hearsay](constructs/Exphearsay.md)
    * [Plain From + の・こと](constructs/Nokoto.md)
    * [...でも and ...って](constructs/Demo.md)
* [Vocabulary](vocab/README.md)
    * [Adjectives](vocab/Adjectives.md)
    * [Verbs](vocab/Verbs.md)
    * [Days of the month](vocab/Days.md)
    * [Counters](vocab/Counters.md)
    * [Family](vocab/Family.md)
    * [Hiragana](vocab/50sounds.md)
    * [New words](vocab/Newwords.md)