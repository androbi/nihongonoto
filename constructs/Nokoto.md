###  Plain From + の・こと

#### Verbs

Verbs are nominalized (from plain form) with の or こと:  

* 走る **の・こと** は 私の趣味 だ / です  
* 泳ぐ **の・こと** はお母さんの趣味 だ / です  
* 私  は / が 歌う **の・こと** が 好（だ / です)  
* 車を洗う **の・こと** は 楽しい（です)  
* 友達と遊ぶ **の・こと** は 面白い（です）  
* 私はスパイン語を話すことができます　　

Notes:

* こと with だ / です
* の with 聞く and 見る; 止める; 待つ and 手伝う
* see https://www.imabi.net/nominalization.htm

#### Adjectives

な: 退屈 **な**  の・こと は 嫌い（だ / です）  
い: 危ない の・こと は 楽しくない（です）  
私はスパイン語を話すことができます

#### ~って

Cidra **って**何ですか。りんごのおさけ**のこと**です。