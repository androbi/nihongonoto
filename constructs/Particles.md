### Particles

#### が particle

##### が as [subject marker](https://www.imabi.net/theparticlegai.htm)

Present **new information** in the form of neutral statements.

* Existential
    * えんぴつがある。
    * 鳥がいる
    * 魚が｛ある・いる｝
* Neutral statements
    * （あなたは）頭がいい。
* 5 senses
    * 変な音がする。
* Intransitive sentences
    * 雨が降ります。

Exhaustive listing, it is X that..

* 彼が学生です。
* asking questions どこ,だれ,なに、いつ、何時
* answers to questions 「何がいい？」「ラーメンがいいでしょう。」

##### が as [object marker](https://www.imabi.net/theparticlegaii.htm)

**stative-transitive predicates of objective fact**

* Possession
    * 私たち（に）は十分なお金かねがあります。
    * 私【には・は】）兄がいます。
    * pets: 私は犬を飼っています。
* Necessity (要る - いる)
    * 乗り物はお金が要ります。
* Non-intentional Perception
* Non-intentional Understanding
    * 日本語が分りますか。
* Ability
    * 日本語が出来ますか。

**stative-transitive predicates of subjective emotions**

* Internal Feeling 
* Like/Dislike
    * んな料理が好ですか。
    * 私は彼が好です。
* Want/Desire
    * どちらがほしいですか。
* Competence
    * 日本語が（お）上手ですね。 (your japanese)
    * 私わたしは料理が得意です。

Expression for competence:

* good:
    * 上手だ (じょうず)
    * うまい
    * 得意だ (とくい)
* bad:
    * 苦手 (にがて)
    * まずい (subjective)
    * 下手 (へただ,, often rude)

#### だけ

The adverbial particle だけ shows limitation meaning "just/only."
https://www.imabi.net/theparticledake.htm

* 赤いリンゴ**だけ**五つください。
* 私は英語**だけ**(を)勉強します。
* 一人だけ｛です・います｝。