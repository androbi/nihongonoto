### Adjective + する/なる

〈Adj + する〉  →  transitive verb
* Adj I + する <br> たかい + する → たかくする(ponerlo alto)
* Adj NA  + する <br> きれいな + する → きれいにする(ponerlo limpio)    


〈Adj + なる〉 → intransitive verb　
* Adj I + なる <br>たかい + なる → たかくなる(ponerse alto)
* Adj NA  + なる <br>  きれいな + なる → きれいになる(ponerse guapa)    


See: [Guide to japanese](http://www.guidetojapanese.org/learn/grammar/surunaru)