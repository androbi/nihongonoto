# Constructs

* [Particles](Particles.md)
* [に 行く/来る](niiku.md)
* [Giving](Giving.md)
* [Adj+する/なる](Surunaru.md)
* [Compound sentences](Compound.md)
* [Experience and hearsay](Exphearsay.md)
* [Plain From + の・こと](Nokoto.md)
* [...でも and ...って](Demo.md)