### Giving あげる、くれる、やる、もらう

See https://www.imabi.net/givingverbs.htm, 美也子 chapter 10.

verb | meaning | に
----|----|---
あげる | I -> others, my group -> my group | recipent
くれる | others -> me, others -> my group | recipent
もらう | to receive | giver


* 父は母に時計を**あげました**
* 私は彼にプレゼントを**あげた**
* 父は私にコンピューターゲームを**くれました**
* 犬に肉を**やった**
* 私は友達に本お**もらいました**