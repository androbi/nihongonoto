### Adjective conjugation

See https://www.imabi.net/adjectives.htm (い adjectives, Keiyōshi) and https://www.imabi.net/adjectivesii.htm (な adjectives, Keiyōdōshi).

#####  ~い - adjectives plain

|+|-|
|-----|-----|
|~い|~くない|
|~かった|~くなかった|

##### ~い - adjectives polite

+|-
---|---
~いです|~くないです ・ ~くありません 
~かったです|~くなかったです ・ ~くありませんでした

polite: add です

exception: いい is conjugated as よい

##### な  - adjectives plain

Only the copula is conjugated. When using before a noun だ → な.

|+|-|
|---|---|
|~|~[では ・ じゃ]ない|
|~だった|~[では ・ じゃ]なかった|

##### な  - adjectives polite

+|-
---|---
~です|~[では ・ じゃ]ありません
~でした|~[では ・じゃ]ありませんでした

Use plain forms to modify nouns.

discussions
* [polite negative forms](http://japanese.stackexchange.com/questions/2574/is-じゃないです-equally-correct-as-じゃありません): ではありません is more negative, [では/じゃ]ないです is more polite
