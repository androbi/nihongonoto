
### Verb conjugation

##### Verbs, polite
use [ます-stem](#masu-stem) + sufix:

+|-
---|---
~ます|~ません
~ました|~ませんでした

##### Verbs, plain
neg: [ない-form](#nai-form), past:[て/た-form](#te-form)


う/る|+|-
---|---|---
う|行く|行かない
  |行った|行かなかった
る|食べる|食べない
 |食べた|食べなかった

##### Exceptions

verb|past|neg.
---|---|---
する | した|しない
くる | きた|こない
ある | あった|ない
